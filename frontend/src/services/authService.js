import client from './client';

export const authUserService = (request) => (
  client({
    url: 'api/auth/token',
    method: 'POST',
    useAccessToken: false,
    body: `username=${request.username}&password=${request.password}`,
  })
);

export const registrationUserService = (request) => (
  client({
    url: 'api/auth/registration',
    method: 'POST',
    useAccessToken: false,
    body: `username=${request.username}&password=${request.password}`,
  })
);
