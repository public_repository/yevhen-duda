import React from 'react';
import configureMockStore from 'redux-mock-store';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import OpenedGames from '../../../components/GameCreate/OpenedGames';
import initialStateCreate from '../initialStateCreate';

const middlewares = [];
const mockStore = configureMockStore(middlewares);

test('games count', () => {
  const counts = [
    0,
    1,
    Math.round(Math.random() * 100) + 1,
    Math.round(Math.random() * 100) + 1,
    Math.round(Math.random() * 100) + 1,
  ];
  counts.forEach((count) => {
    const games = new Array(count).fill({
      players_count: 2,
      yourProps: {},
      opponents: [{
        name: 'opponent',
        color: 'black',
        virus: '0',
        remaining_move_chunks: 3,
        result: '',
      }],
      board_height: 10,
      board_width: 10,
    }).map((value, index) => ({
      ...value, id: index,
    }));

    const initialState = initialStateCreate({ openGame: { games } });
    const store = mockStore(initialState);

    const { container } = render(<Provider store={store}><OpenedGames /></Provider>);
    const rowsCount = container.querySelectorAll('tbody>tr').length;
    expect(rowsCount).toBe(2 * count);
  });
});

test('user are joined to game', () => {
  const initialState = initialStateCreate(
    {
      OpenGame: {
        id: 1,
        board_height: 10,
        board_width: 10,
        game: [{
          players_count: 2,
          yourProps: {},
          opponents: [{
            name: 'a',
            color: 'black',
            virus: '0',
            remaining_move_chunks: 3,
            result: '',
          }],
        }],
      },
    },
  );
  const store = mockStore(initialState);

  const { container } = render(<Provider store={store}><OpenedGames /></Provider>);
  const rowsCount = container.querySelectorAll('tbody>tr').length;
  expect(rowsCount).toBe(0);
});
