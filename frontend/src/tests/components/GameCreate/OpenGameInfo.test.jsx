import React from 'react';
import configureMockStore from 'redux-mock-store';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import OpenGameInfo from '../../../components/GameCreate/OpenGameInfo';
import initialStateCreate from '../initialStateCreate';

const middlewares = [];
const mockStore = configureMockStore(middlewares);

function gameCreate(game = {}) {
  return {
    id: 1,
    yourProps: {},
    opponents: [{
      name: 'opponent',
      color: 'black',
      virus: '0',
      result: '',
      remaining_move_chunks: 3,
    }],
    players_count: 2,
    board_height: 10,
    board_width: 10,
    ...game,
  };
}

test('opponent username', () => {
  const initialState = initialStateCreate();
  const store = mockStore(initialState);

  const opponents = [{
    name: 'username of opponent',
    color: 'black',
    virus: '0',
    result: '',
    remaining_move_chunks: 3,
  }];
  const game = gameCreate({ opponents });

  const { getByText } = render(
    <Provider store={store}>
      <table>
        <tbody>
          <OpenGameInfo game={game} />
        </tbody>
      </table>
    </Provider>,
  );
  expect(getByText(opponents[0].name)).toBeInTheDocument();
});
