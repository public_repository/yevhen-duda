import React from 'react';
import configureMockStore from 'redux-mock-store';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import Creator from '../../../components/GameCreate/Creator';
import initialStateCreate from '../initialStateCreate';

const middlewares = [];
const mockStore = configureMockStore(middlewares);

test('create button', () => {
  const initialState = initialStateCreate();
  const store = mockStore(initialState);

  const { getByText } = render(<Provider store={store}><Creator /></Provider>);
  expect(getByText('Create new game')).toBeInTheDocument();
});

test('no create button if game created', () => {
  const initialState = initialStateCreate();
  const store = mockStore(initialState);

  const { container } = render(<Provider store={store}><Creator /></Provider>);
  expect(container.querySelector('button')).toBe(null);
});

test('fetching create a game', () => {
  const initialState = initialStateCreate({ openGame: { isFetchingGameCreate: true } });
  const store = mockStore(initialState);

  const { container } = render(<Provider store={store}><Creator /></Provider>);
  expect(container.querySelector('.spinner-border')).toBeInTheDocument();
});
