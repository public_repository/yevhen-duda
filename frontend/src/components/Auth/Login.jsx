import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { requestLoginAction } from '../../actions/auth';

function handleSubmit(event, props) {
  event.preventDefault();
  const username = event.target.username.value.trim();
  const password = event.target.password.value.trim();
  const data = { username, password };
  props.dispatch(requestLoginAction(data));
}

function Login(props) {
  const { isFetching } = props;

  return (
    <form onSubmit={(event) => handleSubmit(event, props)}>
      <input
        type="text"
        name="username"
        className="form-control"
        style={{ marginTop: '10px' }}
        placeholder="Username"
        required
      />
      <input
        type="password"
        name="password"
        className="form-control"
        style={{ marginTop: '10px' }}
        placeholder="Password"
        required
      />
      {!isFetching && (
        <input
          type="submit"
          className="form-control btn btn-primary"
          value="Login"
          style={{ marginTop: '10px' }}
        />
      )}
      {isFetching && (
        <div className="spinner-border text-primary" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      )}
    </form>
  );
}

Login.propTypes = {
  isFetching: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  isFetching: state.auth.isFetching,
});

export default connect(mapStateToProps)(Login);
