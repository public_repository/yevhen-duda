import React, { useState } from 'react';
import PropTypes from 'prop-types';

export default function Selector(props) {
  const {
    name, options, bannedOptions, defaultClass, stileFactory, childrenFactory,
  } = props;

  const allowedOptions = options.filter((opt) => bannedOptions.indexOf(opt) === -1);

  const [selected, setOption] = useState(allowedOptions[0]);

  return (
    <div>
      <input type="hidden" name={name} value={selected} />
      {allowedOptions.map((option) => (
        <span
          role="button"
          tabIndex="0"
          className={`btn ${defaultClass} ${selected === option && 'checked'}`}
          style={stileFactory(option)}
          key={`pick_${name}_${option}`}
          onClick={() => (setOption(option))}
          onKeyPress={() => (setOption(option))}
        >
          {childrenFactory({ option, selected })}
        </span>
      ))}
    </div>
  );
}

Selector.propTypes = {
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.string).isRequired,
  childrenFactory: PropTypes.func.isRequired,
  bannedOptions: PropTypes.arrayOf(PropTypes.string),
  stileFactory: PropTypes.func,
  defaultClass: PropTypes.string,
};

Selector.defaultProps = {
  bannedOptions: [],
  stileFactory: () => {},
  defaultClass: '',
};
