import React from 'react';
import Row from './Row';
import './Board.css';
import { gameType } from '../../types';

export default function Board(props) {
  const { game } = props;
  const { yourProps, opponents } = game;
  const rows = game.board;

  const maxSize = Math.max(rows.length, rows[0].length);
  let boardSizeStyle = 'board-sm ';
  if (maxSize > 10) {
    boardSizeStyle = 'board-xlg';
  } else if (maxSize > 8) {
    boardSizeStyle = 'board-lg';
  } else if (maxSize > 6) {
    boardSizeStyle = 'board-m';
  }

  const isYourTurn = game.is_active && yourProps.remaining_move_chunks > 0;

  return (
    <div className={`board ${boardSizeStyle}`}>
      { rows.slice(0).reverse().map(
        (row) => (
          <Row
            gameId={game.id}
            isYourTurn={isYourTurn}
            yourProps={yourProps}
            opponents={opponents}
            cells={row}
            key={`row_${row[0].row}`}
          />
        ),
      )}
    </div>
  );
}

Board.propTypes = {
  game: gameType.isRequired,
};
