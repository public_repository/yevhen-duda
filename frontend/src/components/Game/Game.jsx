import React from 'react';
import { Alert } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useSocket } from '../../context/socketContext';
import Board from './Board';
import Timer from './Timer';
import { gameType } from '../../types';
import './Game.css';

export default function Game(props) {
  const { makeMove } = useSocket();
  const { game } = props;
  const { yourProps, opponents } = game;

  const isExistGame = Object.keys(game).length > 0;

  const maxSize = isExistGame ? Math.max(game.board.length, game.board[0].length) : -1;
  let containerStyle = { display: 'flex', flexDirection: 'column', justifyContent: 'center' };
  let boardSizeStyle = 'board-sm ';
  let boardContainer = { marginTop: '10px' };
  if (maxSize > 10) {
    boardSizeStyle = 'board-xlg';
    containerStyle = {};
    boardContainer = {
      marginTop: '10px',
      maxHeight: '80vh',
      overflowX: 'auto',
      overflowY: 'auto',
    };
  } else if (maxSize > 8) {
    boardSizeStyle = 'board-lg';
  } else if (maxSize > 6) {
    boardSizeStyle = 'board-m';
  }

  const isBeginnedGame = isExistGame && game.is_active;
  const isYourTurn = isBeginnedGame ? yourProps.remaining_move_chunks > 0 : false;
  const result = isExistGame ? yourProps.result : '';
  const activePlayer = !isBeginnedGame ? undefined : [...opponents, yourProps].filter(
    (player) => player.remaining_move_chunks > 0,
  )[0];
  const remainingMoveChunks = activePlayer !== undefined ? activePlayer.remaining_move_chunks : 0;

  const showPassButton = isBeginnedGame && !game.is_ended && isYourTurn && game.can_pass_move;

  return (
    <div style={containerStyle}>
      <div className="game-info">
        {!isExistGame && (
          <Alert variant="danger">
            <Alert.Heading>Game not founded</Alert.Heading>
          </Alert>
        )}
        {result !== '' && (
          <Alert variant="success">
            <Alert.Heading>{result}</Alert.Heading>
            <p>
              Your opponents was&ensp;
              <b>{opponents.map((op) => op.name).join(', ')}</b>
            </p>
          </Alert>
        )}
        <div className="row">
          <div className="col">
            {isExistGame && !game.is_ended && (
              <span style={{ marginLeft: '30px' }}>
                {(isBeginnedGame && `Turn of ${activePlayer.name} (${remainingMoveChunks})`)
                  || 'Waiting for the opponents to join'}
              </span>
            )}
            {isBeginnedGame && !game.is_ended && (
              <Timer
                dateTimeLimit={game.datetime_limit_for_move}
                isYourTurn={isYourTurn}
              />
            )}
          </div>
          <div className="col">
            {showPassButton && (
              <button
                type="button"
                className="btn btn-warning"
                style={{ marginLeft: '30px' }}
                onClick={() => makeMove({ gameId: game.id, row: '', col: '' })}
              >
                PASS
              </button>
            )}
          </div>
        </div>
      </div>
      <div style={boardContainer}>
        {game.board && <Board game={game} className={boardSizeStyle} />}
      </div>
    </div>
  );
}

Game.propTypes = {
  game: PropTypes.oneOfType([gameType, PropTypes.shape({})]).isRequired,
};
