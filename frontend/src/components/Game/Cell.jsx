import React from 'react';
import PropTypes from 'prop-types';
import { useSocket } from '../../context/socketContext';
import Virus from '../Virus/Virus';
import { cellType, playerShortType } from '../../types';

export default function Cell(props) {
  const { makeMove } = useSocket();
  const {
    gameId, cell, isYourTurn, yourProps, opponents,
  } = props;

  const isReachable = isYourTurn && cell.is_reachable_for_virus[yourProps.virus];
  let color = 'black';
  const [owner] = [...opponents, yourProps].filter((player) => player.virus === cell.virus);
  if (owner) {
    color = owner.color;
  }

  return (
    <div
      className="col cell"
      onClick={() => {
        if (isYourTurn) {
          makeMove({ gameId, row: cell.row, col: cell.col });
        }
      }}
      onKeyPress={() => {
        if (isYourTurn) {
          makeMove({ gameId, row: cell.row, col: cell.col });
        }
      }}
      role="button"
      tabIndex="0"
      style={{ backgroundColor: isReachable ? 'rgba(0, 100, 0, 0.6)' : '' }}
    >
      {cell.virus !== '' && (
        <Virus
          type={cell.virus}
          isAlive={!cell.is_captured}
          colorVirus={color}
          className="cell-content"
        />
      )}
    </div>
  );
}

Cell.propTypes = {
  cell: cellType.isRequired,
  gameId: PropTypes.number.isRequired,
  isYourTurn: PropTypes.bool.isRequired,
  yourProps: playerShortType.isRequired,
  opponents: PropTypes.arrayOf(playerShortType).isRequired,
};
