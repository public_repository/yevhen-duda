import React from 'react';
import { useHistory } from 'react-router-dom';
import { gameType } from '../../types';
import Virus from '../Virus/Virus';

export default function GameInfo({ game }) {
  const { yourProps, opponents } = game;

  const isActiveGame = game.is_active;
  const opponentList = opponents.map((op) => op.name).join(', ');

  const history = useHistory();

  return (
    <tr onClick={() => { history.push(`/game/${game.id}`); }} style={{ cursor: 'pointer' }}>
      <td>{`${game.board.length}x${game.board[0].length}`}</td>
      <td><Virus type={yourProps.virus} colorVirus={yourProps.color} /></td>
      <td>{opponentList}</td>
      <td>
        <div className="row">
          {opponents.map((opponent) => (
            <div className="col-2" key={opponent.name}>
              <Virus
                type={opponent.virus}
                colorVirus={opponent.color}
                className="game-info-virus"
              />
            </div>
          ))}
          <div className="col" />
        </div>
      </td>
      <td>
        {!isActiveGame && !game.is_ended ? '-' : (
          (yourProps.remaining_move_chunks > 0 && 'Your')
          || opponents.find((op) => op.remaining_move_chunks > 0).name
        )}
      </td>
    </tr>
  );
}

GameInfo.propTypes = {
  game: gameType.isRequired,
};
