import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { gameType } from '../../types';
import GameInfo from './GameInfo';

function GameList(props) {
  const { games } = props;

  const activeGames = games.filter(
    (game) => !game.is_ended,
  ).sort(
    (first, second) => !first.is_active && second.is_active,
  );

  return (
    <div style={{ marginTop: '10px' }}>
      {activeGames.length > 0 && (
        <table className="table game-list" style={{ fontSize: 'min(1rem, 3vw)' }}>
          <thead>
            <tr>
              <th style={{ width: '10em' }}>Board size</th>
              <th style={{ width: '10em' }}>Your virus</th>
              <th>Opponents</th>
              <th style={{ width: '16em' }}>Viruses of opponents</th>
              <th style={{ width: '10em' }}>Turn</th>
            </tr>
          </thead>
          <tbody>
            {activeGames.map((game) => (
              <GameInfo
                game={game}
                key={`game_${game.id}`}
              />
            ))}
          </tbody>
        </table>
      )}
      {activeGames.length === 0 && 'You don\'t have active games.'}
    </div>
  );
}

GameList.propTypes = {
  games: PropTypes.arrayOf(gameType).isRequired,
};

const mapStateToProps = (state) => ({
  games: state.game.games,
});

export default connect(mapStateToProps)(GameList);
