import React from 'react';
import PropTypes from 'prop-types';

export default function AliveCrossVirus(props) {
  const {
    size, sizeType, color, className,
  } = props;

  const wFactor = 1.1;
  const hFactor = 0.2;

  const xTranslate = -0.5 * (wFactor - 1) * size;
  const yTranslate = 0.5 * (1 - hFactor) * size;

  return (
    <div
      className={className}
      style={{
        width: `${size}${sizeType}`,
        height: `${size}${sizeType}`,
      }}
    >
      <div
        className={className}
        style={{
          position: 'relative',
          width: `${wFactor * size}${sizeType}`,
          height: `${hFactor * size}${sizeType}`,
          margin: '0 0 0 0',
          backgroundColor: color,
          transform: [
            `translateX(${xTranslate}${sizeType})`,
            `translateY(${yTranslate}${sizeType})`,
            'rotate(45deg)',
          ].join(' '),
        }}
      >
        &nbsp;
      </div>
      <div
        className={className}
        style={{
          position: 'relative',
          width: `${wFactor * size}${sizeType}`,
          height: `${hFactor * size}${sizeType}`,
          margin: '0 0 0 0',
          backgroundColor: color,
          transform: [
            `translateX(${xTranslate}${sizeType})`,
            `translateY(${yTranslate - hFactor * size}${sizeType})`,
            'rotate(-45deg)',
          ].join(' '),
        }}
      >
        &nbsp;
      </div>
    </div>
  );
}

AliveCrossVirus.defaultProps = {
  sizeType: 'px',
  color: 'black',
  className: '',
};

AliveCrossVirus.propTypes = {
  size: PropTypes.number.isRequired,
  sizeType: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
};
