import React from 'react';
import PropTypes from 'prop-types';

export default function CircleVirus(props) {
  const {
    size, sizeType, color, className,
  } = props;

  return (
    <div
      className={className}
      style={{
        width: `${size}${sizeType}`,
        height: `${size}${sizeType}`,
        background: color,
        borderRadius: '50%',
      }}
    >
      &nbsp;
    </div>
  );
}

CircleVirus.defaultProps = {
  sizeType: 'px',
  color: 'black',
  className: '',
};

CircleVirus.propTypes = {
  size: PropTypes.number.isRequired,
  sizeType: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
};
