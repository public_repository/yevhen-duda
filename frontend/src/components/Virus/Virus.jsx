import React from 'react';
import PropTypes from 'prop-types';
import AliveCircleVirus from './AliveCircleVirus';
import AliveCrossVirus from './AliveCrossVirus';
import AliveTriangleVirus from './AliveTriangleVirus';
import AliveRhombusVirus from './AliveRhombusVirus';
import CircleVirus from './CircleVirus';
import TriangleVirus from './TriangleVirus';
import CrossVirus from './CrossVirus';
import RhombusVirus from './RhombusVirus';

const viruses = {
  0: { alive: AliveCrossVirus, dead: CrossVirus },
  1: { alive: AliveCircleVirus, dead: CircleVirus },
  2: { alive: AliveRhombusVirus, dead: RhombusVirus },
  3: { alive: AliveTriangleVirus, dead: TriangleVirus },
};

export const virusColors = {
  black: 'black',
  red: '#c42929',
  orange: 'orange',
  green: 'green',
  purple: 'purple',

};

function sizeProcessing(size) {
  return {
    sizeValue: parseFloat(size.slice(0, size.search(/[a-z]/g)), 10),
    sizeType: size.slice(size.search(/[a-z]/g)),
  };
}

export default function Virus(props) {
  const {
    type, size, colorVirus, colorExact, isAlive, className,
  } = props;

  const VirusType = viruses[type][isAlive ? 'alive' : 'dead'];
  const { sizeValue, sizeType } = sizeProcessing(size);

  return (
    <VirusType
      size={sizeValue}
      sizeType={sizeType}
      color={colorVirus ? virusColors[colorVirus] : colorExact}
      className={className}
    />
  );
}

Virus.defaultProps = {
  size: '1em',
  colorVirus: '',
  colorExact: 'black',
  isAlive: true,
  className: '',
};

Virus.propTypes = {
  type: PropTypes.string.isRequired,
  size: PropTypes.string,
  colorVirus: PropTypes.string,
  colorExact: PropTypes.string,
  isAlive: PropTypes.bool,
  className: PropTypes.string,
};
