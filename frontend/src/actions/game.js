import {
  GAMES_DOWNLOAD_SUCCESS,
  GAME_DOWNLOAD_SUCCESS,
} from './types';

export function receiveGameListDownloadAction(games) {
  return {
    type: GAMES_DOWNLOAD_SUCCESS,
    games,
  };
}

export function receiveGameDownloadAction(game) {
  return {
    type: GAME_DOWNLOAD_SUCCESS,
    game,
  };
}
