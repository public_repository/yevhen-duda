import {
  STATISTIC_REQUEST,
  STATISTIC_SUCCESS,
} from './types';

export function requestStatisticAction(pageIndex = 1) {
  return {
    type: STATISTIC_REQUEST,
    isFetching: true,
    pageIndex,
  };
}

export function receiveStatisticAction(creds) {
  return {
    type: STATISTIC_SUCCESS,
    isFetching: false,
    gamesCount: creds.games_count,
    pagesCount: creds.pages_count,
    page: creds.page,
  };
}
