import {
  GAME_LIST_REQUEST,
  GAME_LIST_SUCCESS,
  GAME_JOIN_REQUEST,
  GAME_JOIN_SUCCESS,
  GAME_CREATE_REQUEST,
  GAME_CREATE_SUCCESS,
  GAME_CREATE_FAIL,
} from './types';

export function requestGameListAction() {
  return {
    type: GAME_LIST_REQUEST,
  };
}

export function receiveGameListAction(games) {
  return {
    type: GAME_LIST_SUCCESS,
    games,
  };
}

export function requestJoinGameAction(creds) {
  return {
    type: GAME_JOIN_REQUEST,
    id: creds.id,
    virus: creds.virus,
    color: creds.color,
  };
}

export function receiveJoinGameAction(game) {
  return {
    type: GAME_JOIN_SUCCESS,
    game,
  };
}
export function requestCreateGameAction(game) {
  return {
    type: GAME_CREATE_REQUEST,
    isFetchingGameCreate: true,
    virus: game.virus,
    color: game.color,
    boardHeight: game.boardHeight,
    boardWidth: game.boardWidth,
    playersNumber: game.playersNumber,
  };
}

export function receiveCreateGameAction(game) {
  return {
    type: GAME_CREATE_SUCCESS,
    isFetchingGameCreate: false,
    game,
  };
}

export function errorCreateGameAction() {
  return {
    type: GAME_CREATE_FAIL,
    isFetchingGameCreate: false,
  };
}
