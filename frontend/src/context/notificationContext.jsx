import React, { createContext, useContext } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { addNotificationAction, removeNotificationAction } from '../actions/notifications';

const NotificationContext = createContext();

export const useNotifications = () => useContext(NotificationContext);

function CreateNotificationValue() {
  const dispatch = useDispatch();

  const removeNotification = (notificationID) => {
    dispatch(removeNotificationAction(notificationID));
  };

  const addNotification = (payload) => {
    dispatch(addNotificationAction(payload));
  };

  return {
    addNotification, removeNotification,
  };
}

export default function NotificationProvider({ children }) {
  const value = CreateNotificationValue();
  return (
    <NotificationContext.Provider value={value}>
      {children}
    </NotificationContext.Provider>
  );
}

NotificationProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
