import {
  call, put, takeLatest,
} from 'redux-saga/effects';
import {
  GAME_LIST_REQUEST,
  GAME_CREATE_REQUEST,
  GAME_JOIN_REQUEST,
} from '../actions/types';
import {
  receiveGameListAction,
  receiveCreateGameAction,
  receiveJoinGameAction,
  errorCreateGameAction,
} from '../actions/openGame';
import {
  openGamesDownloadService,
  openGameCreateService,
  openGameJoinService,
} from '../services/openGameService';
import { addNotificationAction } from '../actions/notifications';

function* openGamesDownload() {
  try {
    const games = yield call(openGamesDownloadService);
    yield put(receiveGameListAction(games));
  } catch (e) {
    yield put(addNotificationAction({ message: e.message }));
  }
}

export function* openGameDownloadSaga() {
  yield takeLatest(GAME_LIST_REQUEST, openGamesDownload);
}

function* openGameCreate(creds) {
  try {
    const game = yield call(openGameCreateService, creds);
    yield put(receiveCreateGameAction(game));
    yield put(addNotificationAction({ type: 'success', message: 'The game was created' }));
  } catch (e) {
    yield put(addNotificationAction({ message: e.message }));
    yield put(errorCreateGameAction());
  }
}

export function* openGameCreateSaga() {
  yield takeLatest(GAME_CREATE_REQUEST, openGameCreate);
}

function* openGameJoin(creds) {
  try {
    const game = yield call(openGameJoinService, creds);
    yield put(receiveJoinGameAction(game));
    yield put(addNotificationAction({ type: 'success', message: 'You are joined to game' }));
  } catch (e) {
    yield put(addNotificationAction({ message: e.message }));
  }
}

export function* openGameJoinSaga() {
  yield takeLatest(GAME_JOIN_REQUEST, openGameJoin);
}
