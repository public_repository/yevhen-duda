import {
  call, put, takeLatest, takeEvery,
} from 'redux-saga/effects';
import { LOGIN_REQUEST, LOGOUT, REGISTER_REQUEST } from '../actions/types';
import {
  receiveLoginAction,
  receiveRegistrationAction,
  authErrorAction,
} from '../actions/auth';
import { addNotificationAction } from '../actions/notifications';
import {
  authUserService,
  registrationUserService,
} from '../services/authService';

function* loginUser(creds) {
  try {
    localStorage.setItem('username', creds.username);
    const user = yield call(authUserService, creds);
    localStorage.setItem('access_token', user.access_token);
    yield put(receiveLoginAction(user));
  } catch (e) {
    localStorage.removeItem('username');
    yield put(addNotificationAction({ message: e.message }));
    yield put(authErrorAction());
  }
}

function* logoutUser() {
  localStorage.removeItem('access_token');
  localStorage.removeItem('username');
  yield;
}

export function* loginSaga() {
  yield takeLatest(LOGIN_REQUEST, loginUser);
}

export function* logoutSaga() {
  yield takeEvery(LOGOUT, logoutUser);
}

function* registrationUser(creds) {
  try {
    localStorage.setItem('username', creds.username);
    const user = yield call(registrationUserService, creds);
    localStorage.setItem('access_token', user.access_token);
    yield put(receiveRegistrationAction(user));
  } catch (e) {
    localStorage.removeItem('username');
    yield put(addNotificationAction({ message: e.message }));
    yield put(authErrorAction());
  }
}

export function* registrationSaga() {
  yield takeLatest(REGISTER_REQUEST, registrationUser);
}
