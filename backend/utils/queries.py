from sqlalchemy import null

from backend.models import Game, Player, User
from backend.representations import statistic_page_representation


def game_statistic(
    user: User, page_index: int = 0, games_count_in_page: int = 10,
    db_session=None
):
    gamesQuery = Game.query if db_session is None else db_session
    gamesQuery = gamesQuery.join(Player).filter(
        Game.end_game_date_time != null(),
        user.id == Player.user_id
    )

    total_games_count: int = gamesQuery.count()
    if total_games_count < games_count_in_page*page_index:
        raise ValueError("Incorrect page index")

    offset: int = games_count_in_page*page_index
    games = [
        game for game in gamesQuery.order_by(
            Game.end_game_date_time.desc()
        ).offset(offset).limit(games_count_in_page)
    ]
    return statistic_page_representation(
        games, page_index, games_count_in_page, total_games_count
    )
