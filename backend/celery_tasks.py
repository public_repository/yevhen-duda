import datetime

from celery import Celery
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from backend.config import Config
from backend.models import Game
from backend.utils.queries import game_statistic
from backend.utils.socket import send_data, send_game_to_all_players
from backend.enums import SocketEvent


class FlaskCelery(Celery):
    def init_app(self, flask_app):
        class ContextTask(self.Task):
            abstract = True

            def __call__(self, *args, **kwargs):
                with flask_app.app_context():
                    return super().__call__(*args, **kwargs)

        self.main = flask_app.import_name
        self.conf.update(flask_app.config)
        self.Task = ContextTask


celery = FlaskCelery()


@celery.task()
def check_delay(game_id, move_beginning_date_time):
    move_beginning_date_time =\
        datetime.datetime.fromisoformat(move_beginning_date_time)

    engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)
    session = Session(engine, expire_on_commit=False)
    with session.begin():
        game = session.query(Game).get(game_id)

        if game is not None and game.is_active and (
                game.move_beginning_date_time == move_beginning_date_time
        ):
            game.actualize()
            session.add(game)
            if game.is_active:
                check_delay.apply_async(
                    (game_id, game.move_beginning_date_time.isoformat(),),
                    countdown=Game.MOVE_DURATION.seconds,
                )
            send_game_to_all_players(game, emit_to_queue=True)
        game_ended = game.is_ended
    if game_ended:
        with session.begin():
            game = session.query(Game).get(game_id)
            for player in game.players:
                send_data(
                    SocketEvent.DOWNLOAD_GAME_STATISTIC,
                    {"statistic": game_statistic(
                        player.user, db_session=session.query(Game)
                    )},
                    room=player.user.id,
                    emit_to_queue=True,
                )
