from flask import session
from flask_socketio import join_room, leave_room

from backend.exceptions import SocketException
from backend.middlewares.socket import Socket, get_user_or_emit_error
from backend.models import Game, db
from backend.utils.celery import run_move_time_inspector
from backend.utils.socket import (send_game, send_game_to_all_players,
                                  send_games, send_statistic_first_page,
                                  send_statistic_to_players)

socketio = Socket(
    cors_allowed_origins='*',
)


@socketio.register()
def disconnect(user):
    leave_room(user.id)
    del session['user_id']


@socketio.register()
def download_game(user, request):
    game: Game = Game.query.get(int(request['game_id']))
    if game is None or\
            user.id not in [player.user_id for player in game.players]:
        raise SocketException("Incorrect id of game", user.id)
    game.actualize()
    db.session.commit()
    send_game(game, user.id)


@socketio.register()
def download_games(user, request):
    active_games = []
    games = user.get_games()
    for game in games:
        game.actualize()
        if not game.is_ended:
            active_games.append(game)
        db.session.commit()
    send_games(active_games, user.id)


@socketio.register()
def do_chunk_of_move(user, request):
    game: Game = Game.query.get(int(request['game_id']))
    if game is None:
        raise SocketException("Incorrect game", user.id)
    if not game.is_players_complete:
        raise SocketException("Game is not beginned", user.id)
    if game.active_player.user_id != user.id:
        raise SocketException("Is not your Turn", user.id)
    if request["row"] == "" and request["col"] == "":
        if not game.is_allowed_pass_move():
            raise SocketException("You can't pass move", user.id)
        else:
            game.pass_move()
            db.session.commit()
            run_move_time_inspector(game)
            send_game_to_all_players(game)
            if game.is_ended:
                send_statistic_to_players(game)
            return
    try:
        active_player = game.active_player
        row = int(request["row"])
        col = int(request["col"])
        game.make_chunk_of_move(row, col)
        if game.is_active and active_player != game.active_player:
            run_move_time_inspector(game)
    except ValueError:
        raise SocketException("Incorrect move", user.id)
    send_game_to_all_players(game)
    if game.is_ended:
        send_statistic_to_players(game)


@socketio.register(get_user_or_emit_error, skip_auth_check=True)
def connection(user, request):
    session['user_id'] = user.id
    join_room(user.id)
    download_games.__wrapped__(user, request)
    send_statistic_first_page(user)
