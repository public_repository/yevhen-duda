from functools import wraps
from typing import Callable

from flask import session
from flask_jwt_extended.view_decorators import verify_jwt_in_request
from flask_socketio import SocketIO
from jwt import DecodeError, decode

from backend.config import Config
from backend.enums import SocketEvent
from backend.exceptions import SocketException
from backend.models import User
from backend.utils.socket import send_notification


def socket_exception_handler(
    event: str = SocketEvent.NOTIFICATION
) -> Callable:
    def decorator(f: Callable) -> Callable:
        @wraps(f)
        def handler(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except SocketException as e:
                send_notification(e.message, e.rooms)
        return handler
    return decorator


def get_user_or_emit_error(f: Callable):
    @wraps(f)
    def check_token(*args, **kwargs):
        verify_jwt_in_request(optional=True)
        if not args:
            raise SocketException('access token not finded')
        try:
            data = decode(
                args[0]['access_token'],
                Config.JWT_SECRET_KEY,
                algorithms=["HS256"]
            )
        except DecodeError:
            raise SocketException('can\'t decode access token')

        current_user = User.query.get(data['sub'])
        if current_user is None:
            raise SocketException('user is undefined')

        return f(current_user, *args, **kwargs)
    return check_token


def auth_required(f: Callable):
    @wraps(f)
    def extract_user(*args, **kwargs):
        user_id = session.get('user_id')
        if user_id is None:
            raise SocketException('You are not authenticated')

        user = User.query.get(user_id)
        if user is None:
            raise SocketException('user is undefined')

        return f(user, *args, **kwargs)
    return extract_user


class Socket(SocketIO):
    def on(self, message, namespace=None):
        raise NotImplementedError(
            "This method is not implemented. Use method 'register'."
        )

    def register(self, *decs, skip_auth_check=False,
                 skip_socket_exception_handling=False):
        def decorator(func: Callable) -> Callable:
            event_name: str = func.__name__

            @wraps(func)
            def _handler(sid, *args):
                return self._handle_event(
                    handler=func, message=event_name, namespace='/', sid=sid
                )

            decorator_list = [*decs]
            if not skip_auth_check:
                decorator_list.append(auth_required)
            if not skip_socket_exception_handling:
                decorator_list.append(
                    socket_exception_handler('notification')
                )
            f = func
            for decorator in decorator_list:
                f = decorator(f)
            super(Socket, self).on(event_name)(f)
            return _handler
        return decorator
